import soco
import feedparser
import datetime
import pytemperature
import urllib.request
import json
from gtts import gTTS

weatherURI = "http://api.openweathermap.org/data/2.5/weather?zip=54701,us&appid=e6a2d3ca034f234f2978a9cf5e6a7d3f"

# Weather
with urllib.request.urlopen(weatherURI) as url:
    weatherData = json.loads(url.read().decode())

highTemp = weatherData["main"]["temp_max"]
lowTemp = weatherData["main"]["temp_min"]
currentTemp = weatherData["main"]["temp"]
weatherDesc = ""

for desc in weatherData['weather']:
    weatherDesc += desc['description'] + ", "

weatherString = "The current temperature is " + str(int(pytemperature.k2f(currentTemp))) + " degrees. "
weatherString += "The high today is " + str(int(pytemperature.k2f(highTemp))) + " "
weatherString += "with a low of " + str(int(pytemperature.k2f(lowTemp))) + ". "
weatherString += "The conditions are " + weatherDesc + "."

# Date
date = datetime.datetime.now().strftime("%-I:%M%p on %A, %B %-d")

# Create MP3
mp3Root = "/tmp"
mp3Name = "/morning-info.mp3"

tts = gTTS(text="Good morning! It's " + date + ". " + weatherString, lang='en-uk')
tts.save(mp3Root + mp3Name)

# Find newest podcast

feed = feedparser.parse("https://www.npr.org/rss/podcast.php?id=510318")

targetSpeaker = None
speakers = soco.discover()

# Display a list of speakers
for speaker in speakers:
    if speaker.player_name.lower() == "office":
        targetSpeaker = speaker

internalHost = "http://192.168.1.5:8000"

if targetSpeaker:
    targetSpeaker.volume = 15
    targetSpeaker.play_uri(internalHost + mp3Name)
